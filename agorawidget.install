<?php

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Implements hook_install().
 */
function agorawidget_install() {
  if (\Drupal::moduleHandler()->moduleExists('entity_usage')) {
    // Make sure the "Usage" local task is enabled for agorawidget entities.
    $entity_usage_config = \Drupal::configFactory()->getEditable('entity_usage.settings');
    $tabs_enabled = $entity_usage_config->get('local_task_enabled_entity_types');
    if (is_array($tabs_enabled) && !in_array('agorawidget', $tabs_enabled)) {
      $tabs_enabled[] = 'agorawidget';
      $entity_usage_config->set('local_task_enabled_entity_types', $tabs_enabled)
        ->save();
    }
  }
}

/**
 * Add 'custom_css_classes' field to 'agorawidget' entities.
 */
function agorawidget_update_8101(&$sandbox) {
  $storage_definition = BaseFieldDefinition::create('string')
    ->setLabel(t('Custom CSS classes'))
    ->setDescription(t('Custom CSS classes added to the widget parent element.'))
    ->setSettings([
      'max_length' => 255,
      'text_processing' => 0,
    ])
    ->setTranslatable(FALSE)
    ->setRevisionable(TRUE)
    ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
    ->setDisplayOptions('form', [
      'type' => 'string_textfield',
      'weight' => -4,
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', FALSE);

  \Drupal::entityDefinitionUpdateManager()
    ->installFieldStorageDefinition('custom_css_classes', 'agorawidget', 'agorawidget', $storage_definition);
}
