<?php

namespace Drupal\agorawidget\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class AgorawidgetTypeForm.
 *
 * @package Drupal\agorawidget\Form
 */
class AgorawidgetTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $agorawidget_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $agorawidget_type->label(),
      '#description' => $this->t("Label for the Content widget type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $agorawidget_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\agorawidget\Entity\AgorawidgetType::load',
      ],
      '#disabled' => !$agorawidget_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $agorawidget_type = $this->entity;
    $status = $agorawidget_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('Created the %label Content widget type.', [
          '%label' => $agorawidget_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addStatus($this->t('Saved the %label Content widget type.', [
          '%label' => $agorawidget_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($agorawidget_type->toUrl('collection'));
  }

}
