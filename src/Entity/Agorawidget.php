<?php

namespace Drupal\agorawidget\Entity;

use Drupal\agorawidget\AgorawidgetInterface;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Content widget entity.
 *
 * @ingroup agorawidget
 *
 * @ContentEntityType(
 *   id = "agorawidget",
 *   label = @Translation("Content widget"),
 *   bundle_label = @Translation("Content widget type"),
 *   handlers = {
 *     "storage" = "Drupal\Core\Entity\Sql\SqlContentEntityStorage",
 *     "view_builder" = "Drupal\agorawidget\AgorawidgetViewBuilder",
 *     "list_builder" = "Drupal\agorawidget\AgorawidgetListBuilder",
 *     "views_data" = "Drupal\agorawidget\Entity\AgorawidgetViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\agorawidget\Entity\Form\AgorawidgetForm",
 *       "add" = "Drupal\agorawidget\Entity\Form\AgorawidgetForm",
 *       "edit" = "Drupal\agorawidget\Entity\Form\AgorawidgetForm",
 *       "delete" = "Drupal\agorawidget\Entity\Form\AgorawidgetDeleteForm",
 *     },
 *     "access" = "Drupal\agorawidget\AgorawidgetAccessControlHandler",
 *   },
 *   base_table = "agorawidget",
 *   data_table = "agorawidget_data",
 *   revision_table = "agorawidget_revision",
 *   revision_data_table = "agorawidget_data_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer content widget entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "label" = "name",
 *     "bundle" = "type",
 *     "langcode" = "langcode",
 *     "uuid" = "uuid"
 *   },
 *   bundle_entity_type = "agorawidget_type",
 *   links = {
 *     "canonical" = "/admin/agorawidget/{agorawidget}/edit",
 *     "edit-form" = "/admin/agorawidget/{agorawidget}/edit",
 *     "delete-form" = "/admin/agorawidget/{agorawidget}/delete",
 *     "collection" = "/admin/agorawidget"
 *   },
 *   field_ui_base_route = "entity.agorawidget_type.edit_form"
 * )
 */
class Agorawidget extends ContentEntityBase implements AgorawidgetInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getType() {
    return $this->bundle();
  }

  /**
   * @inheritDoc
   */
  public function getCustomCssClasses() {
    return array_map(function($field_value) {
      return $field_value['value'];
    }, $this->get('custom_css_classes')->getValue());
  }

  /**
   * @inheritDoc
   */
  public function setCustomCssClasses(array $custom_css_classes) {
    $this->set('custom_css_classes', $custom_css_classes);
    return $this;
  }

  /**
   * @inheritDoc
   */
  public function hasCustomCssClasses() {
    return !$this->get('custom_css_classes')->isEmpty();
  }

  /**
   * {@inheritdoc}
   */
  public function hasCustomCssClass($class) {
    if (!$this->hasCustomCssClasses()) {
      return FALSE;
    }
    $classes = $this->getCustomCssClasses();
    return in_array($class, $classes);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Content widget entity.'))
      ->setReadOnly(TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the Content widget entity.'))
      ->setReadOnly(TRUE);

    $fields['vid'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Revision ID'))
      ->setDescription(t('The node revision ID.'))
      ->setReadOnly(TRUE)
      ->setSetting('unsigned', TRUE);

    $fields['type'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Type'))
      ->setDescription(t('The Content widget type.'))
      ->setSetting('target_type', 'agorawidget_type')
      ->setReadOnly(TRUE);

    $fields['langcode'] = BaseFieldDefinition::create('language')
      ->setLabel(t('Language'))
      ->setDescription(t('The language code for the Content widget entity.'))
      ->setTranslatable(TRUE)
      ->setRevisionable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'language_select',
        'weight' => 2,
      ]);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Content widget entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('Drupal\node\Entity\Node::getCurrentUserId')
      ->setTranslatable(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Content widget entity.'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setTranslatable(TRUE)
      ->setRevisionable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['custom_css_classes'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Custom CSS classes'))
      ->setDescription(t('Custom CSS classes added to the widget parent element.'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setPropertyConstraints('value', [
        'Regex' => [
//          'pattern' => '/^[a-zA-Z0-9_\-\u00A1-\uFFFF]+$/',
          'pattern' => '/^[a-zA-Z0-9_\-]+$/',
          'message' => t('Please provide a valid CSS class name.'),
        ],
      ])
      ->setTranslatable(FALSE)
      ->setRevisionable(TRUE)
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 99,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
