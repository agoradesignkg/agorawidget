<?php

namespace Drupal\agorawidget\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Content widget entities.
 */
class AgorawidgetViewsData extends EntityViewsData implements EntityViewsDataInterface {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['agorawidget']['table']['base'] = [
      'field' => 'id',
      'title' => $this->t('Content widget'),
      'help' => $this->t('The Content widget ID.'),
    ];

    return $data;
  }

}
