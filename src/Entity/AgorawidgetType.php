<?php

namespace Drupal\agorawidget\Entity;

use Drupal\agorawidget\AgorawidgetTypeInterface;
use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Content widget type entity.
 *
 * @ConfigEntityType(
 *   id = "agorawidget_type",
 *   label = @Translation("Content widget type"),
 *   handlers = {
 *     "access" = "Drupal\agorawidget\AgorawidgetTypeAccessControlHandler",
 *     "list_builder" = "Drupal\agorawidget\AgorawidgetTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\agorawidget\Form\AgorawidgetTypeForm",
 *       "edit" = "Drupal\agorawidget\Form\AgorawidgetTypeForm",
 *       "delete" = "Drupal\agorawidget\Form\AgorawidgetTypeDeleteForm"
 *     }
 *   },
 *   config_prefix = "agorawidget_type",
 *   bundle_of = "agorawidget",
 *   admin_permission = "administer content widget entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *   },
 *   links = {
 *     "edit-form" = "/admin/structure/agorawidget_type/{agorawidget_type}",
 *     "delete-form" = "/admin/structure/agorawidget_type/{agorawidget_type}/delete",
 *     "collection" = "/admin/structure/agorawidget_type"
 *   }
 * )
 */
class AgorawidgetType extends ConfigEntityBundleBase implements AgorawidgetTypeInterface {
  /**
   * The Content widget type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Content widget type label.
   *
   * @var string
   */
  protected $label;

}
