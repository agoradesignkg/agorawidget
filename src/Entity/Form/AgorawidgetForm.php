<?php

namespace Drupal\agorawidget\Entity\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Content widget edit forms.
 *
 * @ingroup agorawidget
 */
class AgorawidgetForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $status = $entity->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('Created the %label Content widget.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        $this->messenger()->addStatus($this->t('Saved the %label Content widget.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.agorawidget.edit_form', ['agorawidget' => $entity->id()]);
  }

}
