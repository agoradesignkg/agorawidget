<?php

namespace Drupal\agorawidget\Controller;

use Drupal\agorawidget\AgorawidgetTypeInterface;
use Drupal\Core\Controller\ControllerBase;

/**
 * Class AgorawidgetController.
 *
 * @package Drupal\agorawidget\Controller
 */
class AgorawidgetController extends ControllerBase {

  /**
   * Provides the content widget submission form.
   *
   * @param \Drupal\agorawidget\AgorawidgetTypeInterface $agorawidget_type
   *   The content widget type entity for the content widget.
   *
   * @return array
   *   A content widget submission form.
   */
  public function add(AgorawidgetTypeInterface $agorawidget_type) {
    /** @noinspection PhpDeprecationInspection */
    $widget = $this->entityTypeManager()->getStorage('agorawidget')->create([
      'type' => $agorawidget_type->id(),
    ]);

    $form = $this->entityFormBuilder()->getForm($widget);

    return $form;
  }

  /**
   * Displays add content links for available content widget types.
   *
   * Redirects to agorawidget/add/[agorawidget_type] if only one content widget
   * type is available.
   *
   * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
   *   A render array for a list of the widget types that can be added;
   *   if there is only one widget type defined for the site, the function
   *   will return a RedirectResponse to the widget add page for that one
   *   content widget type.
   */
  public function addPage() {
    $content = [];

    // Only use content widget types the user has access to.
    /** @noinspection PhpDeprecationInspection */
    foreach ($this->entityTypeManager()->getStorage('agorawidget_type')->loadMultiple() as $type) {
      /*
       * @todo check create access, similar like:
       * if ($this->entityTypeManager()->getAccessControlHandler('agorawidget')
       * ->createAccess($type->id())).
       */
      $content[$type->id()] = $type;
    }

    // Bypass the agorawidget/add listing if only one content type is available.
    if (count($content) == 1) {
      $type = array_shift($content);
      return $this->redirect('entity.agorawidget.add_form', ['agorawidget_type' => $type->id()]);
    }

    return [
      '#theme' => 'agorawidget_add_list',
      '#content' => $content,
    ];
  }

  /**
   * The _title_callback for the entity.agorawidget.add_form route.
   *
   * @param \Drupal\agorawidget\AgorawidgetTypeInterface $agorawidget_type
   *   The content widget type entity for the content widget.
   *
   * @return string
   *   The page title.
   */
  public function addPageTitle(AgorawidgetTypeInterface $agorawidget_type) {
    return $this->t('Create @name', ['@name' => $agorawidget_type->label()]);
  }

}
