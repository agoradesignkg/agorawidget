<?php

namespace Drupal\agorawidget;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityViewBuilder;

/**
 * Render controller for agorawidget entities.
 */
class AgorawidgetViewBuilder extends EntityViewBuilder {

  /**
   * {@inheritdoc}
   */
  protected function alterBuild(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display, $view_mode) {
    /** @var \Drupal\node\NodeInterface $entity */
    parent::alterBuild($build, $entity, $display, $view_mode);

    if ($entity->id()) {
      $build['#contextual_links']['agorawidget'] = [
        'route_parameters' => ['agorawidget' => $entity->id()],
        'metadata' => ['changed' => $entity->getChangedTime()],
      ];
    }

    if (!empty($build['custom_css_classes'])) {
      $build['custom_css_classes']['#access'] = FALSE;
    }
  }

}
