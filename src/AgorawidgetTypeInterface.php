<?php

namespace Drupal\agorawidget;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Content widget type entities.
 */
interface AgorawidgetTypeInterface extends ConfigEntityInterface {
  // Add get/set methods for your configuration properties here.
}
