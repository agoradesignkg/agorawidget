<?php

namespace Drupal\agorawidget;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Content widget entities.
 *
 * @ingroup agorawidget
 */
interface AgorawidgetInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Gets the rentable item creation timestamp.
   *
   * @return int
   *   The rentable item creation timestamp.
   */
  public function getCreatedTime();

  /**
   * Sets the rentable item creation timestamp.
   *
   * @param int $timestamp
   *   The rentable item creation timestamp.
   *
   * @return $this
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the type (bundle) of the entity.
   *
   * @return string
   *   The bundle of the entity. Defaults to the entity type ID if the entity
   *   type does not make use of different bundles.
   */
  public function getType();

  /**
   * Gets the custom CSS classes.
   *
   * @return string[]
   *   The custom CSS classes.
   */
  public function getCustomCssClasses();

  /**
   * Sets the custom CSS classes.
   *
   * @param string[] $custom_css_classes
   *   The custom CSS classes.
   *
   * @return $this
   */
  public function setCustomCssClasses(array $custom_css_classes);

  /**
   * Gets whether the widget has custom CSS classes.
   *
   * @return bool
   *   TRUE if the widget has custom CSS classes, FALSE otherwise.
   */
  public function hasCustomCssClasses();

  /**
   * Gets whether the widget has a specific custom CSS class set.
   *
   * @param string $class
   *   The CSS class name to search for.
   *
   * @return bool
   *   TRUE if the widget has the request custom CSS class set, FALSE otherwise.
   */
  public function hasCustomCssClass($class);

}
