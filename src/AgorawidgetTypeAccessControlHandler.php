<?php

namespace Drupal\agorawidget;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access controller for the Content widget entity.
 *
 * @see \Drupal\agorawidget\Entity\Agorawidget.
 */
class AgorawidgetTypeAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    switch ($operation) {
      case 'view':
      case 'view label':
      return AccessResult::allowed();

      default:
        return parent::checkAccess($entity, $operation, $account);

    }
  }

}
