<?php

/**
 * @file
 * Contains agorawidget.page.inc.
 *
 * Page callback for Content widget entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Content widget templates.
 *
 * Default template: agorawidget.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_agorawidget(array &$variables) {
  /** @var \Drupal\agorawidget\AgorawidgetInterface $agorawidget */
  $agorawidget = $variables['elements']['#agorawidget'];

  if ($agorawidget->hasCustomCssClasses()) {
    if (!isset($variables['attributes']['class'])) {
      $variables['attributes']['class'] = [];
    }
    $variables['attributes']['class'] = array_merge($variables['attributes']['class'], $agorawidget->getCustomCssClasses());
  }

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }

  if ($agorawidget->hasField('field_image_adj') && !$agorawidget->get('field_image_adj')->isEmpty()) {
    $variables['image_adj'] = $agorawidget->field_image_adj->value;
  }

  // Set title.
  $variables['title'] = $agorawidget->label();
}
